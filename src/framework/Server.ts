import { notFoundResponse } from "../core/controller/utils/ResponseUtility";
import RouteConfig from "./RouteConfig";


function main() {
    const config = require('../../config.json');
    const express = require('express');
    const port = process.env.PORT || config.port;
    const bodyParser = require('body-parser');

    const app = express();


    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.use(express.static(config.static_dir));

    RouteConfig.RegisterRoutes(app);

    app.use((req, res)=>{
        notFoundResponse(res,req.url);
    });

    app.listen(port);
    console.log('financial server started on port: ' + port);
}

main();

