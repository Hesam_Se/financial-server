import "reflect-metadata";
import {Container, decorate, inject, injectable} from "inversify";
import UserController from "../core/controller/UserController";
import BalanceController from "../core/controller/BalanceController";
import LoanController from "../core/controller/LoanController";
import InstallmentController from "../core/controller/InstallmentController";
import IUserUseCase from "../core/usecase/user/IUserUseCase";
import UserUseCase from "../core/usecase/user/UserUseCase";
import IUserRepository from "../core/usecase/user/IUserRepository";
import UserSQliteRepository from "../repository/UserSQliteRepository";
import IBalanceRepository from "../core/usecase/balance/IBalanceRepository";
import BalanceSQliteRepository from "../repository/BalanceSQliteRepository";
import ILoanRepository from "../core/usecase/loan/ILoanRepository";
import LoanSQliteRepository from "../repository/LoanSQliteRepository";
import {Database} from "sqlite3";
import InstallmentSQliteRepository from "../repository/InstallmentSQliteRepository";
import IInstallmentRepository from "../core/usecase/installment/IInstallmentRepository";
import IBalanceUseCase from "../core/usecase/balance/IBalanceUseCase";
import BalanceUseCase from "../core/usecase/balance/BalanceUseCase";
import ILoanUseCase from "../core/usecase/loan/ILoanUseCase";
import LoanUseCase from "../core/usecase/loan/LoanUseCase";
import InstallmentUseCase from "../core/usecase/installment/InstallmentUseCase";
import IInstallmentUseCase from "../core/usecase/installment/IInstallmentUseCase";
import ProfitSQliteRepository from "../repository/ProfitSQliteRepository";
import IProfitRepository from "../core/usecase/profit/IProfitRepository";
import ProfitUseCase from "../core/usecase/profit/ProfitUseCase";
import IProfitUseCase from "../core/usecase/profit/IProfitUseCase";
import ProfitController from "../core/controller/ProfitController";
import ICurrentFinanceReport from "../core/usecase/report/currentFinance/ICurrentFinanceReport";
import CurrentFinanceReport from "../core/usecase/report/currentFinance/CurrentFinanceReport";
import IBankAllFinanceReport from "../core/usecase/report/bankAllFinance/IAllBankFinanceReport";
import BankAllFinanceReport from "../core/usecase/report/bankAllFinance/BankAllFinanceReport";
import ReportController from "../core/controller/ReportController";
import IAllRemainingReport from "../core/usecase/report/allRemaining/IAllRemainingReport";
import AllRemainingReport from "../core/usecase/report/allRemaining/AllRemainingReport";
import IDebtorsReport from "../core/usecase/report/debtors/IDebtorsReport";
import DebtorsReport from "../core/usecase/report/debtors/DebtorsReport";
import CreditorsReport from "../core/usecase/report/creditors/CreditorsReport";
import ICreditorsReport from "../core/usecase/report/creditors/ICreditorsReport";

const DIContainer = new Container({defaultScope:"Singleton",autoBindInjectable:true});

decorate(injectable(),UserController);
decorate(inject("UserUseCase"), UserController, 0);
DIContainer.bind<UserController>("UserController").to(UserController);

decorate(injectable(),BalanceController);
decorate(inject("BalanceUseCase"), BalanceController, 0);
DIContainer.bind<BalanceController>("BalanceController").to(BalanceController);

decorate(injectable(),LoanController);
decorate(inject("LoanUseCase"), LoanController, 0);
DIContainer.bind<LoanController>("LoanController").to(LoanController);

decorate(injectable(),InstallmentController);
decorate(inject("InstallmentUseCase"), InstallmentController, 0);
DIContainer.bind<InstallmentController>("InstallmentController").to(InstallmentController);

decorate(injectable(),ProfitController);
decorate(inject("ProfitUseCase"), ProfitController, 0);
DIContainer.bind<ProfitController>("ProfitController").to(ProfitController);

decorate(injectable(),ReportController);
decorate(inject("BankAllFinanceReport"), ReportController, 0);
decorate(inject("CurrentFinanceReport"), ReportController, 1);
decorate(inject("AllRemainingReport"), ReportController, 2);
decorate(inject("DebtorsReport"), ReportController, 3);
decorate(inject("CreditorsReport"), ReportController, 4);
DIContainer.bind<ReportController>("ReportController").to(ReportController);



decorate(injectable(),UserUseCase);
decorate(inject("UserRepository"),UserUseCase,0);
decorate(inject("BalanceRepository"),UserUseCase,1);
decorate(inject("LoanRepository"),UserUseCase,2);
DIContainer.bind<IUserUseCase>("UserUseCase").to(UserUseCase);

decorate(injectable(),BalanceUseCase);
decorate(inject("BalanceRepository"),BalanceUseCase,0);
DIContainer.bind<IBalanceUseCase>("BalanceUseCase").to(BalanceUseCase);

decorate(injectable(),LoanUseCase);
decorate(inject("LoanRepository"),LoanUseCase,0);
decorate(inject("InstallmentRepository"),LoanUseCase,1);
DIContainer.bind<ILoanUseCase>("LoanUseCase").to(LoanUseCase);

decorate(injectable(),InstallmentUseCase);
decorate(inject("InstallmentRepository"),InstallmentUseCase,0);
decorate(inject("LoanRepository"),InstallmentUseCase,1);
DIContainer.bind<IInstallmentUseCase>("InstallmentUseCase").to(InstallmentUseCase);

decorate(injectable(),ProfitUseCase);
decorate(inject("ProfitRepository"),ProfitUseCase,0);
DIContainer.bind<IProfitUseCase>("ProfitUseCase").to(ProfitUseCase);

decorate(injectable(),CurrentFinanceReport);
decorate(inject("LoanRepository"),CurrentFinanceReport,0);
decorate(inject("ProfitRepository"),CurrentFinanceReport,1);
decorate(inject("BalanceRepository"),CurrentFinanceReport,2);
DIContainer.bind<ICurrentFinanceReport>("CurrentFinanceReport").to(CurrentFinanceReport);

decorate(injectable(),BankAllFinanceReport);
decorate(inject("BalanceRepository"),BankAllFinanceReport,0);
decorate(inject("ProfitRepository"),BankAllFinanceReport,1);
DIContainer.bind<IBankAllFinanceReport>("BankAllFinanceReport").to(BankAllFinanceReport);

decorate(injectable(),AllRemainingReport);
decorate(inject("LoanRepository"),AllRemainingReport,0);
DIContainer.bind<IAllRemainingReport>("AllRemainingReport").to(AllRemainingReport);

decorate(injectable(),DebtorsReport);
decorate(inject("LoanRepository"),DebtorsReport,0);
DIContainer.bind<IDebtorsReport>("DebtorsReport").to(DebtorsReport);

decorate(injectable(),CreditorsReport);
decorate(inject("LoanRepository"),CreditorsReport,0);
DIContainer.bind<ICreditorsReport>("CreditorsReport").to(CreditorsReport);




decorate(injectable(),UserSQliteRepository);
decorate(inject("db"),UserSQliteRepository,0);
DIContainer.bind<IUserRepository>("UserRepository").to(UserSQliteRepository);

decorate(injectable(),BalanceSQliteRepository);
decorate(inject("db"),BalanceSQliteRepository,0);
DIContainer.bind<IBalanceRepository>("BalanceRepository").to(BalanceSQliteRepository);

decorate(injectable(),LoanSQliteRepository);
decorate(inject("db"),LoanSQliteRepository,0);
DIContainer.bind<ILoanRepository>("LoanRepository").to(LoanSQliteRepository);

decorate(injectable(),InstallmentSQliteRepository);
decorate(inject("db"),InstallmentSQliteRepository,0);
DIContainer.bind<IInstallmentRepository>("InstallmentRepository").to(InstallmentSQliteRepository);

decorate(injectable(),ProfitSQliteRepository);
decorate(inject("db"),ProfitSQliteRepository,0);
DIContainer.bind<IProfitRepository>("ProfitRepository").to(ProfitSQliteRepository);


const sqlite3 = require('sqlite3').verbose();
const path = require("../../config.json").database_path;
const db = new sqlite3.Database(path, (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to financial SQlite database.');
});

DIContainer.bind<Database>("db").toConstantValue(db);

export default DIContainer;