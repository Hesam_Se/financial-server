import DIContainer from "./DIContainer";
import UserController from "../core/controller/UserController";
import LoanController from "../core/controller/LoanController";
import BalanceController from "../core/controller/BalanceController";
import InstallmentController from "../core/controller/InstallmentController";
import {Express} from "express";
import ProfitController from "../core/controller/ProfitController";
import ReportController from "../core/controller/ReportController";


class RouteConfig {
    private static app : Express;

    public static RegisterRoutes(app : Express) {
        this.app = app;
        const prefix  = '/api';

        this.userRoutes(prefix);
        this.balanceRoutes(prefix);
        this.loanRoutes(prefix);
        this.installmentRoutes(prefix);
        this.profitRoutes(prefix);
        this.reportRoutes(prefix);
    }

    private static userRoutes(prefix) {
        prefix += '/user';

        const userController = DIContainer.get<UserController>("UserController");

        this.app.route(prefix + '/')
            .get(userController.GetAllUsers)
            .post(userController.AddUser);
        this.app.route(prefix + '/:userId')
            .get(userController.GetUserById)
            .put(userController.EditUser)
            .delete(userController.DeleteUser);
    }

    private static balanceRoutes(prefix) {
        prefix += '/balance';

        const balanceController = DIContainer.get<BalanceController>("BalanceController");

        this.app.route(prefix + '/')
            .post(balanceController.AddBalance);
        this.app.route(prefix + '/:balanceId')
            .put(balanceController.EditBalance)
            .delete(balanceController.DeleteBalance);
        this.app.route(prefix + '/user/:userId')
            .get(balanceController.GetBalanceListByUserId);
    }

    private static loanRoutes(prefix) {
        prefix += '/loan';

        const loanController = DIContainer.get<LoanController>("LoanController");

        this.app.route(prefix + '/')
            .get(loanController.getAllLoans)
            .post(loanController.AddLoan);
        this.app.route(prefix + '/:loanId')
            .get(loanController.getLoanById)
            .put(loanController.updateLoan)
            .delete(loanController.deleteLoan);
        this.app.route(prefix + '/user/:userId')
            .get(loanController.getLoanListByUserId);
    }

    private static installmentRoutes(prefix) {
        prefix += '/installment';

        const installmentController = DIContainer.get<InstallmentController>("InstallmentController");

        this.app.route(prefix + '/')
            .post(installmentController.AddInstallment);
        this.app.route(prefix + '/:installmentId')
            .put(installmentController.EditInstallment)
            .delete(installmentController.DeleteInstallment);
        this.app.route(prefix + '/loan/:loanId')
            .get(installmentController.GetInstallmentListByLoanId);
    }

    private static profitRoutes(prefix) {
        prefix += '/profit';

        const profitController = DIContainer.get<ProfitController>("ProfitController");

        this.app.route(prefix + '/')
            .get(profitController.GetAllProfit)
            .post(profitController.AddProfit);
        this.app.route(prefix + '/:profitId')
            .put(profitController.EditProfit)
            .delete(profitController.DeleteProfit);
        this.app.route(prefix + '/sum/all')
            .get(profitController.SumAllProfit);
    }

    private static reportRoutes(prefix) {
        prefix += '/report';

        const reportController = DIContainer.get<ReportController>("ReportController");

        this.app.route(prefix + '/bankAllFinance')
            .get(reportController.getBankAllFinance);

        this.app.route(prefix + '/currentFinance')
            .get(reportController.getCurrentFinance);

        this.app.route(prefix + '/allRemaining')
            .get(reportController.getAllRemaining);

        this.app.route(prefix + '/debtors')
            .get(reportController.getDebtors);

        this.app.route(prefix + '/creditors')
            .get(reportController.getCreditors);
    }
}

export default RouteConfig;