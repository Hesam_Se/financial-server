import IProfitRepository from "../core/usecase/profit/IProfitRepository";
import Profit from "../core/entity/Profit";
import {Database} from "sqlite3";
import User from "../core/entity/User";

class ProfitSQliteRepository implements IProfitRepository {
    private db : Database;
    constructor(db : Database){
        this.db = db;
    }

    public Add(newProfit: Profit): Promise<number> {
        const values = [newProfit.amount,newProfit.payDate,newProfit.description,newProfit.type];
        const sql = `INSERT INTO Profits(amount,payDate,description,type) 
                      VALUES(?,?,?,?)`;
        return new Promise<number>((resolve,reject) => {
            this.db.run(sql, values, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(this.lastID);
            });
        });
    }

    public Delete(profitId: number): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(`DELETE FROM Profits WHERE id=?`, profitId, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    public Edit(profitId: number,profit: Profit): Promise<boolean> {
        throw new Error("not implemented");
    }

    public SumAllProfits(): Promise<number> {
        return new Promise<number>(resolve => {
            const sql = `SELECT SUM(amount) as sumAll
                        FROM Profits`;

            this.db.get(sql, [], (err, row) => {
                if (err) {
                    throw err;
                }
                resolve(row.sumAll);
            });
        });
    }

    public getAllProfits(): Promise<Profit[]> {
        return new Promise<Profit[]>(resolve => {
            const sql = `SELECT *
            FROM Profits`;

            this.db.all(sql, [], (err, rows) => {
                if (err) {
                    throw err;
                }
                resolve(rows);
            });
        });
    }
}

export default ProfitSQliteRepository;