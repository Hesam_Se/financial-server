import IBalanceRepository from "../core/usecase/balance/IBalanceRepository";
import Balance from "../core/entity/Balance";
import {Database} from "sqlite3";

class BalanceSQliteRepository implements IBalanceRepository {
    private db : Database;
    constructor(db : Database) {
        this.db = db;
    }

    AddBalance(balance: Balance): Promise<number> {
        const sql = `INSERT INTO Balances(userId,amount,transactionNumber,description,createDate) 
                      VALUES(?,?,?,?,?)`;
        const params = [balance.userId,balance.amount,balance.transactionNumber,balance.description,new Date()];
        return new Promise<number>((resolve,reject) => {
            this.db.run(sql,params,function(err) {
                if(err){
                    reject(err);
                }
                resolve(this.lastID);
            });
        });
    }

    DeleteBalance(balanceId: number): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(`DELETE FROM Balances WHERE id=?`, balanceId, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    DeleteBalanceByUserId(userId: number): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(`DELETE FROM Balances WHERE userId=?`, userId, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    EditBalance(balanceId: number, balanceToEdit: Balance): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            reject(new Error("not implemented"));
        });
    }

    GetBalanceListByUserId(userId: number): Promise<Balance[]> {
        const sql = `SELECT * FROM Balances
                        WHERE userId = ?`;
        const params = [userId];
        return new Promise<Balance[]>(resolve => {
            this.db.all(sql, params, (err, rows) => {
                if (err) {
                    throw err;
                }
                resolve(rows);
            });
        });
    }

    SumAllBalances(): Promise<number> {
        const sql = `SELECT SUM(amount) as sumAll
                        FROM Balances
                        `;
        const params = [];
        return new Promise<number>(resolve => {
            this.db.get(sql, params, (err, row) => {
                if (err) {
                    throw err;
                }
                resolve(row.sumAll);
            });
        });
    }
}

export default BalanceSQliteRepository;