import IUserRepository from "../core/usecase/user/IUserRepository";
import User from "../core/entity/User";
import {Database} from "sqlite3";


class UserSQliteRepository implements IUserRepository {
    private db : Database;
    constructor(db : Database){
        this.db = db;
    }
    public AddUser(user: User): Promise<number> {
        const values = [user.firstName,user.lastName,user.nationalId,user.phoneNumber,user.balance,new Date()];
        const sql = `INSERT INTO Users(firstName,lastName,nationalId,phoneNumber,balance,createDate) 
                      VALUES(?,?,?,?,?,?)`;
        return new Promise<number>((resolve,reject) => {
            this.db.run(sql, values, function(err) {
                if (err) {
                     console.log(err.message);
                     reject(err);
                }
                resolve(this.lastID);
            });
        });
    }

    public DeleteUser(userId: number): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(`DELETE FROM Users WHERE id=?`, userId, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    public EditUser(userId : number,userToEdit: User): Promise<boolean> {
        const sql = `UPDATE Users
                     SET firstName=?,
                         lastName=?,
                         nationalId=?,
                         phoneNumber=?
                      WHERE id=?`;
        const params = [userToEdit.firstName,userToEdit.lastName,userToEdit.nationalId,userToEdit.phoneNumber,userId];
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(sql, params, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                if(this.changes !== 1){
                    reject(new Error(this.changes + " row(s) changed."))
                }
                resolve(true);
            });
        });
    }

    public GetAllUsers(): Promise<User[]> {
        return new Promise<User[]>(resolve => {
            const sql = `SELECT Users.* , Loans.id as loanId
            FROM Users LEFT JOIN Loans ON Users.id = Loans.userId`;

            this.db.all(sql, [], (err, rows) => {
                if (err) {
                    throw err;
                }
                resolve(rows);
            });
        });
    }

    public GetUserById(userId: number): Promise<User> {
        const sql = `SELECT Users.* , Loans.id as loanId
            FROM Users LEFT JOIN Loans ON Users.id = Loans.userId
            WHERE Users.id = ?`;
        return new Promise<User>((resolve,reject) => {
            this.db.get(sql, [userId], (err, user) => {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }

                resolve(user);
            });
        });
    }
}

export default UserSQliteRepository;