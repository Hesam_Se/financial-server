import IInstallmentRepository from "../core/usecase/installment/IInstallmentRepository";
import Installment from "../core/entity/Installment";
import {Database} from "sqlite3";
import Loan from "../core/entity/Loan";

class InstallmentSQliteRepository implements IInstallmentRepository {
    private db : Database;
    constructor(db : Database) {
        this.db = db;
    }

    AddInstallment(installment: Installment): Promise<number> {
        const sql = `INSERT INTO Installments(loanId,amount,payDate,transactionNumber,description) 
                      VALUES(?,?,?,?,?)`;
        const params = [installment.loanId,installment.amount,installment.payDate,installment.transactionNumber,installment.description];
        return new Promise<number>((resolve,reject) => {
            this.db.run(sql,params,function(err) {
                if(err){
                    console.error(err);
                    reject(err);
                }
                resolve(this.lastID);
            });
        });
    }

    DeleteInstallment(installmentId: number): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(`DELETE FROM Installments WHERE id=?`, installmentId, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    EditInstallment(installmentId: number, installmentToEdit: Installment): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            reject(new Error("not implemented"));
        });
    }

    GetInstallmentListByLoanId(loanId: number): Promise<Installment[]> {
        const sql = `SELECT * FROM installments
                        WHERE loanId = ?`;
        const params = [loanId];
        return new Promise<Installment[]>(resolve => {
            this.db.all(sql, params, (err, installments) => {
                if (err) {
                    throw err;
                }
                resolve(installments);
            });
        });
    }

    GetInstallmentById(installmentId: number): Promise<Installment> {
        const sql = `SELECT * FROM installments
                        WHERE id = ?`;
        const params = [installmentId];
        return new Promise<Installment>(resolve => {
            this.db.get(sql, params, (err, installment : Installment) => {
                if (err) {
                    throw err;
                }
                resolve(installment);
            });
        });
    }

}
export default InstallmentSQliteRepository;