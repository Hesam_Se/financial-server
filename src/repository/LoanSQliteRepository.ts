import ILoanRepository from "../core/usecase/loan/ILoanRepository";
import Loan from "../core/entity/Loan";
import {Database} from "sqlite3";

class LoanSQliteRepository implements ILoanRepository {
    private db: Database;
    constructor(db : Database) {
        this.db = db;
    }

    AddLoan(loan: Loan): Promise<number> {
        const sql = `INSERT INTO Loans(userId,amount,remain,monthDuration,firstInstallmentAmount,installmentAmount,wageFactor,wageAmount,startDate,status,description) 
                      VALUES(?,?,?,?,?,?,?,?,?,?,?)`;
        const params = [
            loan.userId,
            loan.amount,
            loan.remain,
            loan.monthDuration,
            loan.firstInstallmentAmount,
            loan.installmentAmount,
            loan.wageFactor,
            loan.wageAmount,
            loan.startDate,
            loan.status,
            loan.description
        ];
        return new Promise<number>((resolve,reject) => {
            this.db.run(sql,params,function(err) {
                if(err){
                    console.error(err);
                    reject(err);
                }
                resolve(this.lastID);
            });
        });
    }

    DeleteLoan(loanId: number): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            this.db.run(`DELETE FROM Loans WHERE id=?`, loanId, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    EditLoan(loanId: number, loanToEdit: Loan): Promise<boolean> {
        return new Promise<boolean>((resolve,reject) => {
            reject(new Error("not implemented"));
        });
    }

    getAllLoans(): Promise<Loan[]> {
        return new Promise<Loan[]>(resolve => {
            const sql = `select (Users.firstName || " " || Users.lastName) as username,Loans.*
                            from Users inner join Loans on Loans.userId = Users.id`;

            this.db.all(sql, [], (err, rows) => {
                if (err) {
                    throw err;
                }
                resolve(rows);
            });
        });
    }

    getLoanById(loanId: number): Promise<Loan> {
        const sql = `select (Users.firstName || " " || Users.lastName) as username,Loans.*
                     from Users inner join Loans on Loans.userId = Users.id 
        WHERE Loans.id = ? `;
        return new Promise<Loan>((resolve,reject) => {
            this.db.get(sql, [loanId], (err, loan) => {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(loan);
            });
        });
    }

    getLoanListByUserId(userId: number): Promise<Loan[]> {
        const sql = `select (Users.firstName || " " || Users.lastName) as username,Loans.*
                     from Users inner join Loans on Loans.userId = Users.id
                    WHERE userId = ?`;
        const params = [userId];
        return new Promise<Loan[]>(resolve => {
            this.db.all(sql, params, (err, loans) => {
                if (err) {
                    throw err;
                }
                resolve(loans);
            });
        });
    }

    EditRemain(loanId, addOrSubtractAmount: number): Promise<boolean> {
        const sql = `update Loans
                    set remain = remain + (?)
                    where id = ? 
                    `;
        const params = [addOrSubtractAmount,loanId];

        return new Promise<boolean>((resolve,reject) => {
            this.db.run(sql, params, function(err) {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                if(this.changes !== 1){
                    reject(new Error(this.changes + " row(s) changed."))
                }
                resolve(true);
            });
        });

    }

    sumRemainOpenLoans(): Promise<number> {
        const sql = `select sum(remain) as sumAll
                     from Loans`;
        return new Promise<number>((resolve,reject) => {
            this.db.get(sql, [], (err, row) => {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                resolve(row.sumAll);
            });
        });
    }

}

export default LoanSQliteRepository;