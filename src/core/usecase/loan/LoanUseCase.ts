import ILoanUseCase from "./ILoanUseCase";
import Loan from "../../entity/Loan";
import ILoanRepository from "./ILoanRepository";
import LoanStatus from "../../entity/types/LoanStatus";
import IInstallmentRepository from "../installment/IInstallmentRepository";

class LoanUseCase implements ILoanUseCase{
    private readonly loanRepository : ILoanRepository;
    private readonly installmentRepository : IInstallmentRepository;

    constructor(loanRepository: ILoanRepository,installmentRepository : IInstallmentRepository){
        this.loanRepository = loanRepository;
        this.installmentRepository = installmentRepository;
    }

    public async AddLoan(loan : Loan) : Promise<number> {
        if(await this.userHasOpenLoan(loan.userId))
            throw new Error("کاربر وام باز دارد و نمی تواند وام جدید ایجاد کند.");

        loan.status = LoanStatus.open;
        loan.wageAmount = loan.wageFactor * loan.amount;
        loan.remain = parseInt(loan.amount as any) + parseInt(loan.wageAmount as any);

        return this.loanRepository.AddLoan(loan);
    }

    private async userHasOpenLoan(userId) : Promise<boolean> {
        const loans = await this.loanRepository.getLoanListByUserId(userId);
        const openLoans = loans.filter(l => l.status === LoanStatus.open);
        return openLoans.length > 0;
    }


    public async DeleteLoan(loanId : number) : Promise<boolean> {
        if(await this.loanHasInstallment(loanId))
            throw new Error("برای وام قسط ثبت شده است و نمیتوان آن را حذف کرد.");
        return this.loanRepository.DeleteLoan(loanId);
    }
    private async loanHasInstallment(loanId : number) : Promise<boolean> {
        const installments = await this.installmentRepository.GetInstallmentListByLoanId(loanId);
        return installments.length > 0;
    }

    public async EditLoan(loanId: number, loanToEdit: Loan): Promise<boolean> {
        return this.loanRepository.EditLoan(loanId,loanToEdit);
    }

    public async getAllLoans(): Promise<Loan[]> {
        return this.loanRepository.getAllLoans();
    }

    public async getLoanById(loanId: number): Promise<Loan> {
        return this.loanRepository.getLoanById(loanId);
    }

    public async getLoanListByUserId(userId: number): Promise<Loan[]> {
        return this.loanRepository.getLoanListByUserId(userId);
    }

}

export default LoanUseCase;