import Loan from "../../entity/Loan";

interface ILoanRepository {
    AddLoan(newLoan : Loan) : Promise<number>;
    EditLoan(loanId : number,loanToEdit: Loan) : Promise<boolean>;
    EditRemain(loanId,addOrSubtractAmount : number) : Promise<boolean>;
    DeleteLoan(loanId : number) : Promise<boolean>;
    getLoanById(loanId : number) : Promise<Loan>;
    getLoanListByUserId(userId : number) : Promise<Loan[]>;
    getAllLoans() : Promise<Loan[]>;
    sumRemainOpenLoans() : Promise<number>;
}

export default ILoanRepository;