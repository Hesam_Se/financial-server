import Loan from "../../entity/Loan";

export default interface ILoanUseCase {
    AddLoan(newLoan : Loan) : Promise<number>;
    EditLoan(loanId : number,loanToEdit: Loan) : Promise<boolean>;
    DeleteLoan(loanId : number) : Promise<boolean>;
    getLoanById(loanId : number) : Promise<Loan>;
    getLoanListByUserId(userId : number) : Promise<Loan[]>;
    getAllLoans() : Promise<Loan[]>;
}