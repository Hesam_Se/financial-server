import Profit from "../../entity/Profit";

interface IProfitUseCase {
    Add(newProfit : Profit) : Promise<number>;
    Delete(profitId : number) : Promise<boolean>;
    Edit(profitId : number,profit : Profit) : Promise<boolean>;
    getAllProfits() : Promise<Profit[]>;
    SumAllProfits() : Promise<number>;
}

export default IProfitUseCase;