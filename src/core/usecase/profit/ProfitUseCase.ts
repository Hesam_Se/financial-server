import IProfitUseCase from "./IProfitUseCase";
import Profit from "../../entity/Profit";
import IProfitRepository from "./IProfitRepository";

class ProfitUseCase implements IProfitUseCase{
    private profitRepository : IProfitRepository;

    constructor(profitRepository: IProfitRepository){
        this.profitRepository = profitRepository;
    }

    public async Add(newProfit: Profit): Promise<number> {
        return await this.profitRepository.Add(newProfit);
    }

    public async SumAllProfits(): Promise<number> {
        return await this.profitRepository.SumAllProfits();
    }

    public async getAllProfits(): Promise<Profit[]> {
        return await this.profitRepository.getAllProfits();
    }

    public async Delete(profitId: number): Promise<boolean> {
        return await this.profitRepository.Delete(profitId);
    }

    public async Edit(profitId:number,profit: Profit): Promise<boolean> {
        return await this.profitRepository.Edit(profitId,profit);
    }

}

export default ProfitUseCase;