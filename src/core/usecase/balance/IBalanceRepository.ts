import Balance from "../../entity/Balance";

interface IBalanceRepository {
    GetBalanceListByUserId(userId : number) : Promise<Balance[]>;
    AddBalance(newBalance : Balance) : Promise<number>;
    EditBalance(balanceId : number,balanceToEdit : Balance) : Promise<boolean>;
    DeleteBalance(balanceId : number) : Promise<boolean>;
    DeleteBalanceByUserId(userId : number) : Promise<boolean>;
    SumAllBalances() : Promise<number>;
}


export default IBalanceRepository;