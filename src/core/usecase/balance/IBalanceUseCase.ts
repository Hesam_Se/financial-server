import Balance from "../../entity/Balance";

interface IBalanceUseCase {
    GetBalanceListByUserId(userId : number) : Promise<Balance[]>;
    AddBalance(newBalance : Balance) : Promise<number>;
    EditBalance(balanceId : number,balanceToEdit : Balance) : Promise<boolean>;
    DeleteBalance(balanceId : number) : Promise<boolean>;
}

export default IBalanceUseCase;