import IBalanceUseCase from "./IBalanceUseCase";
import Balance from "../../entity/Balance";
import IBalanceRepository from "./IBalanceRepository";

class BalanceUseCase implements IBalanceUseCase {
    private readonly balanceRepository : IBalanceRepository;

    public constructor(balanceRepository : IBalanceRepository){
        this.balanceRepository = balanceRepository;
    }

    public AddBalance(newBalance: Balance): Promise<number> {
        return this.balanceRepository.AddBalance(newBalance);
    }

    public DeleteBalance(balanceId: number): Promise<boolean> {
        return this.balanceRepository.DeleteBalance(balanceId);
    }

    public EditBalance(balanceId: number, balanceToEdit: Balance): Promise<boolean> {
        return this.balanceRepository.EditBalance(balanceId,balanceToEdit);
    }

    public GetBalanceListByUserId(userId: number): Promise<Balance[]> {
        return this.balanceRepository.GetBalanceListByUserId(userId);
    }
}

export default BalanceUseCase;