import IInstallmentUseCase from "./IInstallmentUseCase";
import Installment from "../../entity/Installment";
import IInstallmentRepository from "./IInstallmentRepository";
import ILoanRepository from "../loan/ILoanRepository";

class InstallmentUseCase implements IInstallmentUseCase {
    private readonly installmentRepository : IInstallmentRepository;
    private readonly loanRepository : ILoanRepository;

    constructor(installmentRepository : IInstallmentRepository,loanRepository : ILoanRepository){
        this.installmentRepository = installmentRepository;
        this.loanRepository = loanRepository;
    }

    async AddInstallment(newInstallment: Installment): Promise<number> {
        const installmentId = await this.installmentRepository.AddInstallment(newInstallment);
        const remainSubtracted = await this.loanRepository.EditRemain(newInstallment.loanId,-newInstallment.amount);

        return installmentId;
    }

    async DeleteInstallment(installmentId: number): Promise<boolean> {
        const installment = await this.installmentRepository.GetInstallmentById(installmentId);
        const isDeleted = await this.installmentRepository.DeleteInstallment(installmentId);
        const remainAdded = await this.loanRepository.EditRemain(installment.loanId,+installment.amount);

        return isDeleted && remainAdded;
    }

    EditInstallment(installmentId: number, installmentToEdit: Installment): Promise<boolean> {
        // todo: calculate remain
        return this.installmentRepository.EditInstallment(installmentId,installmentToEdit);
    }

    GetInstallmentListByLoanId(loanId: number): Promise<Installment[]> {
        return this.installmentRepository.GetInstallmentListByLoanId(loanId);
    }

}
export default InstallmentUseCase;