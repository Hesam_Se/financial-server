import Installment from "../../entity/Installment";

interface IInstallmentUseCase {
    AddInstallment(newInstallment : Installment) : Promise<number>;
    EditInstallment(installmentId : number,installmentToEdit : Installment) : Promise<boolean>;
    DeleteInstallment(installmentId : number) : Promise<boolean>;
    GetInstallmentListByLoanId(loanId : number) : Promise<Installment[]>;
}

export default IInstallmentUseCase;