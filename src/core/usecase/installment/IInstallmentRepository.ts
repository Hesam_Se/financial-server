import Installment from "../../entity/Installment";

interface IInstallmentRepository {
    GetInstallmentListByLoanId(loanId: number) : Promise<Installment[]>;
    GetInstallmentById(installmentId : number) : Promise<Installment>;
    AddInstallment(newInstallment : Installment) : Promise<number>;
    EditInstallment(installmentId : number,installmentToEdit : Installment) : Promise<boolean>;
    DeleteInstallment(installmentId : number) : Promise<boolean>;
}

export default IInstallmentRepository;