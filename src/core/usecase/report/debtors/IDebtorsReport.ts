import Debtor from "./Debtor";

interface IDebtorsReport {
    get() : Promise<Debtor[]>;
}

export default IDebtorsReport;