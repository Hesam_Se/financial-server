import IDebtorsReport from "./IDebtorsReport";
import ILoanRepository from "../../loan/ILoanRepository";
import Debtor from "./Debtor";
import Loan from "../../../entity/Loan";
import * as jmoment from 'jalali-moment';

class DebtorsReport implements IDebtorsReport {
    private readonly loanRepository : ILoanRepository;

    constructor(loanRepository : ILoanRepository) {
        this.loanRepository = loanRepository;
    }


    public async get(): Promise<Debtor[]> {
        const allLoans = await this.loanRepository.getAllLoans();
        const debtors = [] as Debtor[];
        allLoans.forEach(loan => {
                const debt = this.calculateDebt(loan);

                if(debt > 0) {
                    debtors.push(new Debtor(
                        loan.userId,
                        loan.username,
                        loan.id,
                        debt
                    ));
                }
        });
        debtors.sort((a,b) => a.debt < b.debt ? 1 : 0);
        return debtors;
    }

    private calculateDebt(loan : Loan) {
        const months = this.calculateMonth(loan.startDate as any);
        if(months > 0) {
            const shouldPayAmount = loan.firstInstallmentAmount + (loan.installmentAmount * (months - 1));
            const payedAmount = loan.amount + loan.wageAmount - loan.remain;
            return shouldPayAmount - payedAmount;
        }
        return 0;
    }

    private calculateMonth(loanDate : number) : number {
        const currentDate = jmoment(new Date());
        const jloanDate = jmoment(loanDate);
        return currentDate.diff(jloanDate,'months');
    }

}

export default DebtorsReport;