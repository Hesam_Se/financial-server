import Creditor from "./Creditor";

interface ICreditorsReport {
    get() : Promise<Creditor[]>;
}

export default ICreditorsReport;