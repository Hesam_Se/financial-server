import ICreditorsReport from "./ICreditorsReport";
import ILoanRepository from "../../loan/ILoanRepository";
import Creditor from "./Creditor";
import Loan from "../../../entity/Loan";
import * as jmoment from 'jalali-moment';

class CreditorsReport implements ICreditorsReport {
    private readonly loanRepository : ILoanRepository;

    constructor(loanRepository : ILoanRepository) {
        this.loanRepository = loanRepository;
    }


    public async get(): Promise<Creditor[]> {
        const allLoans = await this.loanRepository.getAllLoans();
        const creditors = [] as Creditor[];
        allLoans.forEach(loan => {
            const credit = this.calculateCredit(loan);

            if(credit > 0) {
                creditors.push(new Creditor(
                    loan.userId,
                    loan.username,
                    loan.id,
                    credit
                ));
            }
        });
        creditors.sort((a,b) => a.credit < b.credit ? 1 : 0);
        return creditors;
    }

    private calculateCredit(loan : Loan) {
        const months = this.calculateMonth(loan.startDate as any);
        if(months > 0) {
            const shouldPayAmount = loan.firstInstallmentAmount + (loan.installmentAmount * (months - 1));
            const payedAmount = loan.amount + loan.wageAmount - loan.remain;
            return payedAmount - shouldPayAmount;
        }
        return 0;
    }

    private calculateMonth(loanDate : number) : number {
        const currentDate = jmoment(new Date());
        const jLoanDate = jmoment(loanDate);
        return currentDate.diff(jLoanDate,'months');
    }

}

export default CreditorsReport;