 interface IAllRemainingReport {
     get() : Promise<number>;
 }

 export default IAllRemainingReport;