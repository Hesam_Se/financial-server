import IAllRemainingReport from "./IAllRemainingReport";
import ILoanRepository from "../../loan/ILoanRepository";

class AllRemainingReport implements IAllRemainingReport {
    private readonly loanRepository : ILoanRepository;

    constructor(loanRepository : ILoanRepository){
        this.loanRepository = loanRepository;
    }

    public get(): Promise<number> {
        return this.loanRepository.sumRemainOpenLoans();
    }

}

export default AllRemainingReport;