import ILoanRepository from "../../loan/ILoanRepository";
import ICurrentFinanceReport from "./ICurrentFinanceReport";
import IProfitRepository from "../../profit/IProfitRepository";
import IBalanceRepository from "../../balance/IBalanceRepository";

class CurrentFinanceReport implements ICurrentFinanceReport {
    private readonly loanRepository : ILoanRepository;
    private readonly balanceRepository : IBalanceRepository;
    private readonly profitRepository : IProfitRepository;

    constructor(loanRepository : ILoanRepository,profitRepository : IProfitRepository,balanceRepository : IBalanceRepository) {
        this.loanRepository = loanRepository;
        this.profitRepository = profitRepository;
        this.balanceRepository = balanceRepository;
    }

    public async get(): Promise<number> {
        const sumProfit = this.profitRepository.SumAllProfits();
        const sumBalance = this.balanceRepository.SumAllBalances();
        const sumRemainLoans = this.loanRepository.sumRemainOpenLoans();

        return (await sumProfit + await sumBalance) - await sumRemainLoans;
    }

}

export default CurrentFinanceReport;