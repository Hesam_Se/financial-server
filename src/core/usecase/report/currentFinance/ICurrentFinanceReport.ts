
interface ICurrentFinanceReport {
    get() : Promise<number>;
}

export default ICurrentFinanceReport;