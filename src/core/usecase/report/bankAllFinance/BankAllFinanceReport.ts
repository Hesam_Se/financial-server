
import IBalanceRepository from "../../balance/IBalanceRepository";
import IProfitRepository from "../../profit/IProfitRepository";
import IBankAllFinanceReport from "./IAllBankFinanceReport";

class BankAllFinanceReport implements IBankAllFinanceReport {
    private readonly balanceRepository : IBalanceRepository;
    private readonly profitRepository : IProfitRepository;

    constructor(balanceRepository : IBalanceRepository,profitRepository : IProfitRepository) {
        this.balanceRepository = balanceRepository;
        this.profitRepository = profitRepository;
    }

    public async get(): Promise<number> {
        const allBalances = this.balanceRepository.SumAllBalances();
        const allProfits = this.profitRepository.SumAllProfits();

        return await allBalances + await allProfits;
    }
}

export default BankAllFinanceReport;