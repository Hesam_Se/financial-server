
interface IBankAllFinanceReport {
    get() : Promise<number>;
}

export default IBankAllFinanceReport;