import IUserUseCase from "./IUserUseCase";
import User from "../../entity/User";
import IUserRepository from "./IUserRepository";
import IBalanceRepository from "../balance/IBalanceRepository";
import Balance from "../../entity/Balance";
import ILoanRepository from "../loan/ILoanRepository";

class UserUseCase implements IUserUseCase {
    private readonly userRepository : IUserRepository;
    private readonly balanceRepository : IBalanceRepository;
    private readonly loanRepository : ILoanRepository;

    constructor(userRepository : IUserRepository,balanceRepository : IBalanceRepository,loanRepository : ILoanRepository){
        this.userRepository = userRepository;
        this.balanceRepository = balanceRepository;
        this.loanRepository = loanRepository;
    }

    public async AddUser(user: User) : Promise<number>{
        const userId = await this.userRepository.AddUser(user);
        await this.AddFirstBalance(userId,user.balance);
        return userId;
    }

    private async AddFirstBalance(userId,amount) : Promise<any> {
        const balance = new Balance(0,userId,amount,null,new Date(),"first balance");
        try {
            await this.balanceRepository.AddBalance(balance);
        }
        catch (ex) {
            this.userRepository.DeleteUser(userId);
            throw ex;
        }
    }

    public async DeleteUser(userId : number) : Promise<boolean> {
        if(await this.userHasLoan(userId))
            throw new Error("برای کاربر وام ثبت شده است و نمی توان آن را حذف کرد.");

        if(await this.userHasBalance(userId))
            throw new Error("برای کاربر موجودی ثبت شده است و نمی توان آن را حذف کرد.");

        return this.userRepository.DeleteUser(userId);
    }

    private async userHasLoan(userId) : Promise<boolean> {
        const loans = await this.loanRepository.getLoanListByUserId(userId);
        console.log(loans.length > 0);
        return loans.length > 0;
    }

    private async userHasBalance(userId) : Promise<boolean> {
        const balances = await this.balanceRepository.GetBalanceListByUserId(userId);
        return balances.length > 0;
    }


    public async EditUser(userId,userToEdit: User): Promise<boolean> {
        return this.userRepository.EditUser(userId,userToEdit);
    };

    public GetAllUsers(): Promise<User[]> {
        return this.userRepository.GetAllUsers();
    };

    public GetUserById(userId: number): Promise<User> {
        return this.userRepository.GetUserById(userId);
    };

}

export default UserUseCase;