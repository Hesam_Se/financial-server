import User from "../../entity/User";


interface IUserRepository {
    GetUserById(userId : number) : Promise<User>;
    GetAllUsers() : Promise<User[]>;
    AddUser(user : User) : Promise<number>;
    EditUser(userId : number,userToEdit : User) : Promise<boolean>;
    DeleteUser(userId : number) : Promise<boolean>;
}

export default IUserRepository;