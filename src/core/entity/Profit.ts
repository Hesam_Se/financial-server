import ProfitType from "./types/ProfitType";

export default class Profit {
    public id: number;
    public amount: number;
    public payDate: Date;
    public type: ProfitType;
    public description : string;

    constructor(id: number, amount: number, payDate: Date, type: ProfitType, description: string) {
        this.id = id;
        this.amount = amount;
        this.payDate = payDate;
        this.type = type;
        this.description = description;
    }

}