export default class Balance {
    public id : number;
    public userId : number;
    public amount : number;
    public transactionNumber : string;
    public createDate : Date;
    public description : string;

    constructor(id : number,userId : number,amount : number,transactionNumber : string,createDate : Date,description : string){
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.transactionNumber = transactionNumber;
        this.createDate = createDate;
        this.description = description;
    }
}