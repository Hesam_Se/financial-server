import UserDTO from "../models/DTOs/UserDTO";
import User from "../../entity/User";
import BalanceDTO from "../models/DTOs/BalanceDTO";
import Balance from "../../entity/Balance";
import LoanDTO from "../models/DTOs/LoanDTO";
import Loan from "../../entity/Loan";
import InstallmentDTO from "../models/DTOs/InstallmentDTO";
import Installment from "../../entity/Installment";
import DateUtility from "../utils/DateUtility";
import ProfitDTO from "../models/DTOs/ProfitDTO";
import Profit from "../../entity/Profit";
import LoanUtility from "../utils/LoanUtility";

export default class DtoToEntity {
    public static convertUser(userDto: UserDTO) : User {
        const user = new User(
            userDto.id,
            userDto.firstName,
            userDto.lastName,
            userDto.nationalId,
            userDto.loanId,
            userDto.phoneNumber,
            userDto.balance,
            null,
        );
        return user;
    }

    public static convertBalance(balanceDto: BalanceDTO) : Balance {
        const balance = new Balance(
            balanceDto.id,
            balanceDto.userId,
            balanceDto.amount,
            balanceDto.transactionNumber,
            DateUtility.toGregorianDate(balanceDto.createDate),
            balanceDto.description
        );
        return balance;
    }

    public static convertLoan(loanDTO: LoanDTO) : Loan {
        const loan = new Loan(
            loanDTO.id,
            loanDTO.userId,
            loanDTO.username,
            loanDTO.amount,
            loanDTO.remain,
            loanDTO.monthDuration,
            LoanUtility.calculateFirstInstallment(loanDTO.amount,loanDTO.wageFactor,loanDTO.monthDuration),
            LoanUtility.calculateInstallment(loanDTO.amount,loanDTO.wageFactor,loanDTO.monthDuration),
            loanDTO.wageFactor,
            loanDTO.amount * loanDTO.wageFactor,
            DateUtility.toGregorianDate(loanDTO.startDate),
            loanDTO.status,
            loanDTO.description
        );
        return loan;
    }

    public static convertInstallment(installmentDto: InstallmentDTO) : Installment {
        return new Installment(
            installmentDto.id,
            installmentDto.loanId,
            installmentDto.amount,
            DateUtility.toGregorianDate(installmentDto.payDate),
            installmentDto.transactionNumber,
            installmentDto.description
        )
    }

    public static convertProfit(profitDto: ProfitDTO) : Profit {
        return new Profit(
            profitDto.id,
            profitDto.amount,
            DateUtility.toGregorianDate(profitDto.payDate),
            profitDto.type,
            profitDto.description
        )
    }
}