import UserDTO from "../models/DTOs/UserDTO";
import User from "../../entity/User";
import Balance from "../../entity/Balance";
import BalanceDTO from "../models/DTOs/BalanceDTO";
import Loan from "../../entity/Loan";
import LoanDTO from "../models/DTOs/LoanDTO";
import Installment from "../../entity/Installment";
import InstallmentDTO from "../models/DTOs/InstallmentDTO";
import DateUtility from "../utils/DateUtility";
import Profit from "../../entity/Profit";
import ProfitDTO from "../models/DTOs/ProfitDTO";
import Debtor from "../../usecase/report/debtors/Debtor";
import DebtorDTO from "../models/DTOs/DebtorDTO";
import Creditor from "../../usecase/report/creditors/Creditor";
import CreditorDTO from "../models/DTOs/CreditorDTO";


export default class EntityToDto {
    public static convertUser(user : User) : UserDTO {
        return new UserDTO(
            user.id,
            user.firstName,
            user.lastName,
            user.nationalId,
            user.loanId,
            user.phoneNumber,
            user.balance,
            DateUtility.toJalalidateTime(user.createDate)
        );
    }

    public static convertBalance(balance : Balance) : BalanceDTO {
        return new BalanceDTO(
            balance.id,
            balance.userId,
            balance.amount,
            balance.transactionNumber,
            DateUtility.toJalalidate(balance.createDate),
            balance.description
        )
    }

    public static convertLoan(loan : Loan) : LoanDTO {
        return new LoanDTO(
            loan.id,
            loan.userId,
            loan.username,
            loan.amount,
            loan.remain,
            loan.monthDuration,
            loan.firstInstallmentAmount,
            loan.installmentAmount,
            loan.wageFactor,
            loan.wageAmount,
            DateUtility.toJalalidate(loan.startDate),
            loan.status,
            loan.description
        );
    }

    public static convertInstallment(installment : Installment) : InstallmentDTO {
        return new InstallmentDTO(
            installment.id,
            installment.loanId,
            installment.amount,
            DateUtility.toJalalidate(installment.payDate),
            installment.transactionNumber,
            installment.description
        )
    }

    public static convertProfit(profit: Profit) : ProfitDTO {
        return new ProfitDTO(
            profit.id,
            profit.amount,
            DateUtility.toJalalidate(profit.payDate),
            profit.type,
            profit.description
        )
    }

    public static convertDebtor(debtor: Debtor) : DebtorDTO {
        return new DebtorDTO(
            debtor.userId,
            debtor.username,
            debtor.loanId,
            debtor.debt
        )
    }

    public static convertCreditor(creditor: Creditor) : CreditorDTO {
        return new CreditorDTO(
            creditor.userId,
            creditor.username,
            creditor.loanId,
            creditor.credit
        )
    }
}