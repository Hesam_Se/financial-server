export type ResponseType = 'success' | 'error' | 'warning';
export type DetailsType = string | Object;