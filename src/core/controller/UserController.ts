import IUserUseCase from "../usecase/user/IUserUseCase";
import {errorResponse, successResponse} from "./utils/ResponseUtility";
import UserDTO from "./models/DTOs/UserDTO";
import DtoToEntity from "./converters/DtoToEntity";
import EntityToDto from "./converters/EntityToDto";

class UserController {
    private readonly userUseCase : IUserUseCase;

    constructor(userUseCase : IUserUseCase){
        this.userUseCase = userUseCase;
    }

    public AddUser = (req,res) => {
        const user = DtoToEntity.convertUser(req.body as UserDTO);
        this.userUseCase.AddUser(user)
            .then(userId => {
                successResponse(res,'کاربر با موفقیت ایجاد شد.',null,{userId});
            })
            .catch(err => {
                errorResponse(res,'ایجاد کاربر با خطا مواجه شد.',err.message);
            });
    };

    public DeleteUser = (req,res) => {
        const userId = req.params.userId;
        this.userUseCase.DeleteUser(userId).then(isDeleted => {
            successResponse(res,'کاربر با موفقیت حذف شد.',null,{isDeleted});
        })
            .catch(err => {
                errorResponse(res,'حذف کاربر با خطا مواجه شد.',err.message);
            });
    };

    public EditUser = (req,res) => {
        const userId = req.params.userId;
        const user = DtoToEntity.convertUser(req.body as UserDTO);
        this.userUseCase.EditUser(userId,user)
            .then(isEdited => {
                successResponse(res,'کاربر با موفقیت ویرایش شد.',null,{isEdited});
            })
            .catch(err => {
                errorResponse(res,'ویرایش کاربر با خطا مواجه شد.',err.message);
            });
    };

    public GetAllUsers = (req,res) => {
        this.userUseCase.GetAllUsers()
            .then(userList => {
                const userDtoList = userList.map(user => {
                    return EntityToDto.convertUser(user);
                });
                successResponse(res,'لیست کاربران با موفقیت ارسال شد.',null,userDtoList);
            })
            .catch(err => {
                errorResponse(res,'دریافت لیست کاربران با خطا مواجه شد.',err.message);
            })
    };

    public GetUserById = (req,res) => {
        const userId = req.params.userId;
        this.userUseCase.GetUserById(userId)
            .then(user => {
                successResponse(res,'کاربر با موفقیت ارسال شد.',null,EntityToDto.convertUser(user));
            })
            .catch(err => {
                errorResponse(res,'دریافت کاربر با خطا مواجه شد.',err.message);
            })
    };
}
export default UserController;