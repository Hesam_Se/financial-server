import ILoanUseCase from "../usecase/loan/ILoanUseCase";
import {errorResponse, successResponse} from "./utils/ResponseUtility";
import LoanDTO from "./models/DTOs/LoanDTO";
import DtoToEntity from "./converters/DtoToEntity";
import EntityToDto from "./converters/EntityToDto";

class LoanController {
    private readonly loanUseCase : ILoanUseCase;

    constructor(loanUseCase : ILoanUseCase) {
        this.loanUseCase = loanUseCase;
    }

    public getAllLoans = (req, res) => {
        this.loanUseCase.getAllLoans()
            .then(loanList => {
                const loanDTOList = loanList.map(loan => EntityToDto.convertLoan(loan));
                successResponse(res,'لیست وام ها با موفقیت ارسال شد.',null,loanDTOList);
            })
            .catch(err => {
                errorResponse(res,'دریافت لیست وام ها با خطا مواجه شد.',err.message);
            })
    };

    public AddLoan = (req, res) => {
        const newLoan = DtoToEntity.convertLoan(req.body as LoanDTO);
        this.loanUseCase.AddLoan(newLoan)
            .then(loanId => {
                successResponse(res,'وام با موفقیت ایجاد شد.',null,{loanId});
            })
            .catch(err => {
                errorResponse(res,'ایجاد وام با خطا مواجه شد.',err.message);
            })
    };

    public getLoanById = (req, res) => {
        const loanId = req.params.loanId;
        this.loanUseCase.getLoanById(loanId).then(loan => {
            const loanDto = EntityToDto.convertLoan(loan);
            successResponse(res,'وام با موفقیت ارسال شد.',null,loanDto);
        })
            .catch(err => {
                errorResponse(res,'دریافت وام با خطا مواجه شد.',err.message);
            })
    };

    public getLoanListByUserId = (req, res) => {
        const userId = req.params.userId;
        this.loanUseCase.getLoanListByUserId(userId).then(loanList => {
            const loanDTOList = loanList.map(loan => EntityToDto.convertLoan(loan));
            successResponse(res,'لیست وام های کاربر با موفقیت ارسال شد.',null,loanDTOList);
        })
            .catch(err => {
                errorResponse(res,'دریافت لیست وام های کاربر با خطا مواجه شد.',err.message);
            })
    };

    public updateLoan = (req, res) => {
        const loanId = req.params.loanId;
        const loan = DtoToEntity.convertLoan(req.body as LoanDTO);
        this.loanUseCase.EditLoan(loanId,loan)
            .then(isEdited => {
                successResponse(res,'وام با موفقیت ویرایش شد.',null,{isEdited});
            })
            .catch(err => {
                errorResponse(res,'ویرایش وام با خطا مواجه شد.',err.message);
            })
    };

    public deleteLoan = (req, res) => {
        const loanId = req.params.loanId;
        this.loanUseCase.DeleteLoan(loanId)
            .then(isDeleted => {
                successResponse(res,'وام با موفقیت حذف شد.',null,{isDeleted});
            })
            .catch(err => {
                errorResponse(res,'حذف وام با خطا مواجه شد.',err.message);
            });
    };

}

export default LoanController;