import { ResponseType,DetailsType } from '../types/ResponseTypes';
export class Response {
    public type : ResponseType;
    public code : number;
    public description : string;
    public details : DetailsType;
    public data : any;
}