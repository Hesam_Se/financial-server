
class DebtorDTO {
    public userId: number;
    public username: string;
    public loanId : number;
    public debt : number;

    constructor(userId : number,username : string,loanId : number,debt : number) {
        this.userId = userId;
        this.username = username;
        this.loanId = loanId;
        this.debt = debt;
    }
}

export default DebtorDTO;