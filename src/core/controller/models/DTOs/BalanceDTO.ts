export default class BalanceDTO {
    public id : number;
    public userId : number;
    public amount : number;
    public transactionNumber : string;
    public createDate : string;
    public description : string;

    constructor(id : number,userId : number,amount : number,transactionNumber : string,createDate : string,description : string){
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.transactionNumber = transactionNumber;
        this.createDate = createDate;
        this.description = description;
    }
}