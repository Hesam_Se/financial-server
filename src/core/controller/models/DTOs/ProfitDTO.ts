import ProfitType from "../../../entity/types/ProfitType";

export default class ProfitDTO {
    public id: number;
    public amount: number;
    public payDate: string;
    public type: ProfitType;
    public description : string;

    constructor(id: number, amount: number, payDate: string, type: ProfitType, description: string) {
        this.id = id;
        this.amount = amount;
        this.payDate = payDate;
        this.type = type;
        this.description = description;
    }
}