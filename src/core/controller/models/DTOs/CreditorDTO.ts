
class CreditorDTO {
    public userId: number;
    public username: string;
    public loanId : number;
    public credit : number;

    constructor(userId : number,username : string,loanId : number,credit : number) {
        this.userId = userId;
        this.username = username;
        this.loanId = loanId;
        this.credit = credit;
    }
}

export default CreditorDTO;