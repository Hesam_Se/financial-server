export default class InstallmentDTO {
    public id : number;
    public loanId : number;
    public amount : number;
    public payDate : string;
    public transactionNumber?: number;
    public description?: string;

    constructor(id: number,loanId : number,amount: number,payDate : string,transactionNumber?: number,description?: string) {
        this.id = id;
        this.loanId = loanId;
        this.amount = amount;
        this.payDate = payDate;
        this.transactionNumber = transactionNumber;
        this.description = description;
    }
}