import {Response} from '../models/Response';
import {ResponseType,DetailsType} from '../types/ResponseTypes';


const responseObject = (type : ResponseType,code : number,description : string,details : DetailsType,data : any) : Response => {
    return({
        type,
        code,
        description,
        details,
        data
    });
};



export const successResponse = (response,description : string,details : string,data : any) => {
    response.status(200).json(responseObject('success',+1,description,details,data));
};

export const errorResponse = (response, description : string,details : string) => {
    response.status(500).json(responseObject('error',-1,description,details,null));
};

export const notFoundResponse = (response,url) => {
    response.status(404)
        .json(
            responseObject(
                'error',
                404,
                'مسیر مورد نظر یافت نشد.',
                {
                    error: "404 Not Found!",
                    url
                },
                null
            )
        );
};
