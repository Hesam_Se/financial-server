

export default class LoanUtility {
    public static calculateFirstInstallment(loanAmount: number,wageFactor: 0.01 | 0.02,monthDuration: number) : number {
        console.log(loanAmount,wageFactor,monthDuration);
        const realAmount = parseInt(loanAmount as any) + parseInt((loanAmount * wageFactor) as any);
        console.log("realAmount:",realAmount);

        const realInstallment = realAmount/monthDuration;
        console.log("realInstallment:",realInstallment);

        const integerInstallment = Math.round(realInstallment / 10000) * 10000;
        console.log("integerInstallment",integerInstallment);

        const x = realAmount - (integerInstallment * (monthDuration -1));
        console.log("result:",x);
        return x;

    }

    public static calculateInstallment(loanAmount: number,wageFactor: 0.01 | 0.02,monthDuration: number) : number {
        const realAmount = parseInt(loanAmount as any) + parseInt((loanAmount * wageFactor) as any);
        const realInstallment = realAmount/monthDuration;
        return Math.round(realInstallment / 10000) * 10000;
    }
}