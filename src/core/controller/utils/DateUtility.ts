import * as jmoment from 'jalali-moment';

export default class DateUtility {
    public static toGregorianDate(jalaliDate : string) : Date {
        if(!jalaliDate){
            throw new Error("فرمت تاریخ صحیح نیست.");
        }
        try {
            const date = jmoment.from(jalaliDate,'fa','YY/MM/DD');
            return date.toDate();
        }
        catch(ex) {
            throw new Error("فرمت تاریخ صحیح نیست.");
        }
    }

    public static toJalalidate(date : Date) : string {
        try {
            const string = jmoment(date).format('jYY/jMM/jDD');
            return string;
        }
        catch (e) {
            throw new Error("فرمت تاریخ صحیح نیست.");
        }
    }

    public static toJalalidateTime(datetime : Date) : string {
        try {
            const string = jmoment(datetime).format('jYY/jMM/jDD HH:mm:ss');
            return string;
        }
        catch (e) {
            throw new Error("فرمت تاریخ صحیح نیست.");
        }
    }
}