import IProfitUseCase from "../usecase/profit/IProfitUseCase";
import {errorResponse, successResponse} from "./utils/ResponseUtility";
import ProfitDTO from "./models/DTOs/ProfitDTO";
import DtoToEntity from "./converters/DtoToEntity";
import EntityToDto from "./converters/EntityToDto";

class ProfitController {
    private readonly profitUseCase : IProfitUseCase;

    constructor(profitUseCase : IProfitUseCase) {
        this.profitUseCase = profitUseCase;
    }

    public AddProfit = (req, res) => {
        const profit = DtoToEntity.convertProfit(req.body as ProfitDTO);
        this.profitUseCase.Add(profit)
            .then(profitId => {
                successResponse(res,'سود با موفقیت ایجاد شد.',null,{profitId});
            })
            .catch(err => {
                errorResponse(res,'ایجاد سود با خطا مواجه شد.',err.message);
            })
    };

    public EditProfit = (req, res) => {
        const profitId = req.params.profitId;
        const profit = DtoToEntity.convertProfit(req.body as ProfitDTO);
        this.profitUseCase.Edit(profitId,profit)
            .then(isEdited => {
                successResponse(res,'سود با موفقیت ویرایش شد.',null,{isEdited});
            })
            .catch(err => {
                errorResponse(res,'ویرایش سود با خطا مواجه شد.',err.message);
            })
    };


    public DeleteProfit = (req, res) => {
        const profitId = req.params.profitId;
        this.profitUseCase.Delete(profitId)
            .then(isDeleted => {
                successResponse(res,'سود با موفقیت حذف شد.',null,{isDeleted});
            })
            .catch(err => {
                errorResponse(res,'حذف سود با خطا مواجه شد.',err.message);
            });
    };

    public GetAllProfit = (req, res) => {
        this.profitUseCase.getAllProfits()
            .then(profits => {
                const profitDto = profits.map( p => EntityToDto.convertProfit(p));
                successResponse(res,'لیست سودها با موفقیت ارسال شد.',null,profitDto);
            })
            .catch(err => {
                errorResponse(res,'ارسال لیست سودها با خطا مواجه شد.',err.message);
            });
    };

    public SumAllProfit = (req, res) => {
        this.profitUseCase.SumAllProfits()
            .then(sumAll => {
                successResponse(res,'جمع سودها با موفقیت ارسال شد.',null,{sumAll});
            })
            .catch(err => {
                errorResponse(res,'ارسال جمع سودها با خطا مواجه شد.',err.message);
            });
    };
}

export default ProfitController;