import IInstallmentUseCase from "../usecase/installment/IInstallmentUseCase";
import {errorResponse, successResponse} from "./utils/ResponseUtility";
import InstallmentDTO from "./models/DTOs/InstallmentDTO";
import DtoToEntity from "./converters/DtoToEntity";
import EntityToDto from "./converters/EntityToDto";

class InstallmentController {
    private readonly installmentUseCase : IInstallmentUseCase;

    constructor(installmentUseCase : IInstallmentUseCase) {
        this.installmentUseCase = installmentUseCase;
    }

    public AddInstallment = (req, res) => {
        const installment = DtoToEntity.convertInstallment(req.body as InstallmentDTO);
        this.installmentUseCase.AddInstallment(installment)
            .then(installmentId => {
                successResponse(res,'قسط با موفقیت ایجاد شد.',null,{installmentId});
            })
            .catch(err => {
                errorResponse(res,'ایجاد قسط با خطا مواجه شد.',err.message);
            })
    };

    public EditInstallment = (req, res) => {
        const installmentId = req.params.installmentId;
        const installment = DtoToEntity.convertInstallment(req.body as InstallmentDTO);
        this.installmentUseCase.EditInstallment(installmentId,installment)
            .then(isEdited => {
                successResponse(res,'قسط با موفقیت ویرایش شد.',null,{isEdited});
            })
            .catch(err => {
                errorResponse(res,'ویرایش قسط با خطا مواجه شد.',err.message);
            })
    };

    public GetInstallmentListByLoanId = (req, res) => {
        const loanId = req.params.loanId;
        this.installmentUseCase.GetInstallmentListByLoanId(loanId)
            .then(installmentList => {
                const installmentDtoList = installmentList.map(installment => EntityToDto.convertInstallment(installment));
                successResponse(res,'اقساط وام با موفقیت ارسال شد.',null,installmentDtoList);
            })
            .catch(err => {
                errorResponse(res,'دریافت اقساط وام با خطا مواجه شد.',err.message);
            })
    };

    public DeleteInstallment = (req, res) => {
        const installmentId = req.params.installmentId;
        this.installmentUseCase.DeleteInstallment(installmentId)
            .then(isDeleted => {
                successResponse(res,'قسط با موفقیت حذف شد.',null,{isDeleted});
            })
            .catch(err => {
                errorResponse(res,'حذف قسط با خطا مواجه شد.',err.message);
            });
    };
}

export default InstallmentController;