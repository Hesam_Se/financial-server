import IBalanceUseCase from "../usecase/balance/IBalanceUseCase";
import {errorResponse, successResponse} from "./utils/ResponseUtility";
import BalanceDTO from "./models/DTOs/BalanceDTO";
import DtoToEntity from "./converters/DtoToEntity";
import EntityToDto from "./converters/EntityToDto";

class BalanceController {
    private readonly balanceUseCase : IBalanceUseCase;

    constructor(balanceUseCase : IBalanceUseCase) {
        this.balanceUseCase = balanceUseCase;
    }

    public AddBalance = (req, res) => {
        const balance = DtoToEntity.convertBalance(req.body as BalanceDTO);
        this.balanceUseCase.AddBalance(balance)
            .then(balanceId => {
                successResponse(res,'تراکنش با موفقیت انجام شد.',null,{balanceId});
            })
            .catch(err => {
                errorResponse(res,'ثبت تراکنش با خطا مواجه شد.',err.message);
            })
    };

    public GetBalanceListByUserId = (req,res) => {
        const userId = req.params.userId;
        this.balanceUseCase.GetBalanceListByUserId(userId)
            .then(balanceList => {
                const balanceDtoList = balanceList.map(balance => EntityToDto.convertBalance(balance));
                successResponse(res,'تراکنش های کاربر با موفقیت ارسال شد.',null,balanceDtoList);
            })
            .catch(err => {
                errorResponse(res,'دریافت تراکنش های کاربر با خطا مواجه شد.',err.message);
            })
    };

    public EditBalance = (req, res) => {
        const balanceId = req.params.balanceId;
        const balance = DtoToEntity.convertBalance(req.body as BalanceDTO);
        this.balanceUseCase.EditBalance(balanceId,balance).then(isEdited => {
            successResponse(res,'تراکنش با موفقیت ویرایش شد.',null,{isEdited});
        })
            .catch(err => {
                errorResponse(res,'ویرایش تراکنش با خطا مواجه شد.',err.message);
            })
    };

    public DeleteBalance = (req, res) => {
        const balanceId = req.params.balanceId;
        this.balanceUseCase.DeleteBalance(balanceId).then(isDeleted => {
            successResponse(res,'حذف تراکنش با موفقیت انجام شد.',null,{isDeleted});
        })
            .catch(err => {
                errorResponse(res,'حذف تراکنش با خطا مواجه شد.',err.message);
            });
    };


}

export default BalanceController;