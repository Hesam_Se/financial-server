import IBankAllFinanceReport from "../usecase/report/bankAllFinance/IAllBankFinanceReport";
import ICurrentFinanceReport from "../usecase/report/currentFinance/ICurrentFinanceReport";
import {errorResponse, successResponse} from "./utils/ResponseUtility";
import IAllRemainingReport from "../usecase/report/allRemaining/IAllRemainingReport";
import IDebtorsReport from "../usecase/report/debtors/IDebtorsReport";
import Debtor from "../usecase/report/debtors/Debtor";
import EntityToDto from "./converters/EntityToDto";
import Creditor from "../usecase/report/creditors/Creditor";
import ICreditorsReport from "../usecase/report/creditors/ICreditorsReport";

class ReportController {
    private readonly bankAllFinanceReport : IBankAllFinanceReport;
    private readonly currentFinanceReport : ICurrentFinanceReport;
    private readonly allRemaining : IAllRemainingReport;
    private readonly debtors : IDebtorsReport;
    private readonly creditors: ICreditorsReport;

    constructor(
        bankAllFinanceReport : IBankAllFinanceReport,
        currentFinanceReport : ICurrentFinanceReport,
        allRemaining : IAllRemainingReport,
        debtors : IDebtorsReport,
        creditors : ICreditorsReport,
    ) {
        this.bankAllFinanceReport = bankAllFinanceReport;
        this.currentFinanceReport = currentFinanceReport;
        this.allRemaining = allRemaining;
        this.debtors = debtors;
        this.creditors = creditors;
    }

    public getBankAllFinance = (req,res) => {
        this.bankAllFinanceReport.get()
            .then(bankAllFinance => {
                successResponse(res,'موجودی کل صندوق با موفقیت ارسال شد.',null,bankAllFinance);
            })
            .catch(err => {
                errorResponse(res,'ارسال موجودی کل صندوق با خطا مواجه شد.',err.message);
            });

    };

    public getCurrentFinance = (req,res) => {
        this.currentFinanceReport.get()
            .then(currentFinance => {
                successResponse(res,'موجودی فعلی صندوق با موفقیت ارسال شد.',null,currentFinance);
            })
            .catch(err => {
                errorResponse(res,'ارسال موجودی فعلی صندوق با خطا مواجه شد.',err.message);
            });
    };

    public getAllRemaining = (req,res) => {
        this.allRemaining.get()
            .then(allRemaining => {
                successResponse(res,'کل مطالبات با موفقیت ارسال شد.',null,allRemaining);
            })
            .catch(err => {
                errorResponse(res,'ارسال کل مطالبات با خطا مواجه شد.',err.message);
            })
    };

    public getDebtors = (req,res) => {
        this.debtors.get()
            .then((debtors : Debtor[]) => {
                const debtorDTOs = debtors.map(d => EntityToDto.convertDebtor(d));
                successResponse(res,'لیست بدهکاران با موفقیت ارسال شد.',null,debtorDTOs);
            })
            .catch(err => {
                errorResponse(res,'ارسال لیست بدهکاران با خطا مواجه شد.',err.message);
            })
    };

    public getCreditors = (req,res) => {
        this.creditors.get()
            .then((creditors : Creditor[]) => {
                const creditorDTOs = creditors.map(d => EntityToDto.convertCreditor(d));
                successResponse(res,'لیست بدهکاران با موفقیت ارسال شد.',null,creditorDTOs);
            })
            .catch(err => {
                errorResponse(res,'ارسال لیست بدهکاران با خطا مواجه شد.',err.message);
            })
    };
}

export default ReportController;